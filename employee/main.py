
from sqlalchemy.sql.functions import mode
from models import Employee
from fastapi import FastAPI
from fastapi.exceptions import HTTPException
from fastapi.params import Depends
from sqlalchemy.orm import session
from database import engine, SessionLocal
import schemas, models

app = FastAPI()

fakedb = []

models.Base.metadata.create_all(bind=engine)

def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


@app.post("/employees", status_code=201, tags=['Employees'])
def create(request:schemas.Employee, db: session = Depends(get_db)):

    new_employee = models.Employee(id = request.id, name = request.name, email = request.email, contact_number=request.contact_number)
    db.add(new_employee)
    db.commit()
    db.refresh(new_employee)
    
    return new_employee




@app.get("/employees", tags=['Employees'])
def show_all(db: session = Depends(get_db)):
    
    return db.query(models.Employee).all()



@app.get("/employees/{id}", tags=['Employees'])
def show_Employee(id: int, db: session = Depends(get_db)):
    employee = db.query(models.Employee).filter(models.Employee.id == id).first()
    if not employee:
        raise HTTPException(
             status_code=404,
             detail="Item not found",
        ) 
    return employee





    
@app.delete("/employees/{id}", tags=['Employees'])
def delete(id:int, db: session = Depends(get_db)):
    employee = db.query(models.Employee).filter(models.Employee.id == id).delete(synchronize_session = False)
    if not employee:
        raise HTTPException(
            status_code=404,
            detail="Item not found",
           
        )
    db.commit()
    return "Deleted succesfully"




@app.put("/employees/{id}", status_code=202,  tags=['Employees'])
def update(id:int, request:schemas.Employee, db: session = Depends(get_db)):
    
    employee = db.query(models.Employee).filter(models.Employee.id == id).update({"id" : request.id, "name" : request.name, "email" : request.email, "contact_number":request.contact_number}, synchronize_session = False)

    if not employee:
        raise HTTPException(
            status_code=404,
            detail="Item not found",
           
        )
    
    db.commit()
    
    return "Updated succesfully"