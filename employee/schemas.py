from pydantic import BaseModel
from typing import Optional

class Employee(BaseModel):
    id: int
    name: str
    email: str
    contact_number: Optional[int] = None
