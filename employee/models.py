from database import Base
from sqlalchemy import Column, Integer, String

class Employee(Base):
    __tablename__ = "employee"

    id = Column(Integer, primary_key=True, index=True)
    email = Column(String, index=True)
    name = Column(String)
    contact_number = Column(Integer)

